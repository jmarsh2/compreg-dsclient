FROM ubuntu:18.04

ENV PATH=/data/jdk8u212-b03/bin:$PATH

COPY scripts    /

RUN mkdir /data &&\
    apt-get update &&\
    apt-get install -y curl wget &&\
    curl -L --output /data/OpenJDK8.tar.gz https://github.com/AdoptOpenJDK/openjdk8-binaries/releases/download/jdk8u212-b03/OpenJDK8U-jdk_x64_linux_hotspot_8u212b03.tar.gz &&\
    tar -xf /data/OpenJDK8.tar.gz -C /data &&\
    rm /data/OpenJDK8.tar.gz &&\
    wget -q -O /jetty.tar.gz "https://repo1.maven.org/maven2/org/eclipse/jetty/jetty-distribution/9.4.15.v20190215/jetty-distribution-9.4.15.v20190215.tar.gz" &&\
    tar -xvf /jetty.tar.gz &&\
    rm /jetty.tar.gz &&\
    mv  jetty-distribution-9.4.15.v20190215 jetty &&\
    chmod +x entrypoint.sh &&\
    apt-get autoremove -y &&\
    apt-get clean &&\
    rm /var/lib/apt/lists/*_*

COPY jars       /jetty/lib/ext
COPY webapps    /jetty/webapps

ENTRYPOINT /entrypoint.sh