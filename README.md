# neurocrine-dsclient

# Local build

## Add your Artifactory Username and API key to the .netrc
This will tell wget to fallback to this athentication method after the first fail with HTTP 401
```
touch ~/.netrc
chmod 600 ~/.netrc
echo 'machine artifacts.chemaxon.com login [USERNAME] password [API-KEY]' >> ~/.netrc
```

## Download dsclient war
```
wget -O webapps/dsclient.war https://artifacts.chemaxon.com/artifactory/list/compreg-libs-release-local/com/chemaxon/registration/RegistryCxn_dsclient/20.16.0-2008281630/RegistryCxn_dsclient-20.16.0-2008281630.war
```

## Download the MySQL and Oracle JDBC jars
```
wget -O jars/mysql-connector-java-8.0.13.jar https://artifacts.chemaxon.com:443/artifactory/repo.maven.apache.org-libs-1-remote-cache/mysql/mysql-connector-java/8.0.13/mysql-connector-java-8.0.13.jar

wget -O jars/ojdbc6-11.2.0.4.jar https://artifacts.chemaxon.com:443/artifactory/maven.oracle.com-libs-1-remote-cache/com/oracle/jdbc/ojdbc6/11.2.0.4/ojdbc6-11.2.0.4.jar

```

## Docker build
```
docker build -t neurocrine-dsclient .
```
