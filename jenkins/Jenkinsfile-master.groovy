public class CXNJobConfig {
    public static final String pkg = 'com.chemaxon.consultancy.neurocrine-dsclient'
    public static final String projectShortName = 'neurocrine-dsclient'
    public static final String repoPrefix = 'consultancy'
    public static final String repoStage = 'staging'
    public static String slackThread = '#consultancy-notifications'
}
import static CXNJobConfig.*
import org.jenkinsci.plugins.workflow.steps.FlowInterruptedException

pipeline {
    agent { label '!master' }
    stages {
        stage("Prepare") {
            steps {
                // For debugging purposes, it is always useful to print info
                // about build environment that is seen by shell during the build
                sh 'env'
                script {
                    REVISION = "1.0-" + BUILD_TIMESTAMP
                    server = Artifactory.server 'artifactory'
                    dockerRepo = "consultancy_neurocrine-docker-release-local"
                }
                addShortText(
                        text: REVISION,
                        borderColor: 'transparent'
                )
            }
        }
        stage("Downloading dsclien.war") {
            steps {
                script {
                    def downloadSpec = """{
                    "files": [
                    {
                        "pattern": "compreg-libs-release-local/com/chemaxon/registration/RegistryCxn_dsclient/20.16.0-2008281630/RegistryCxn_dsclient-20.16.0-2008281630.war",
                        "target": "./dsclient.war"
                        }
                    ]
                    }"""
                    server.download(downloadSpec)
                    sh 'mv com/chemaxon/registration/RegistryCxn_dsclient/20.16.0-2008281630/* ./webapps'
                    sh 'rm -rf com'
                }
            }
        }
        stage("Downloading ojdbc6.jar") {
            steps {
                script {
                    def downloadSpec = """{
                    "files": [
                    {
                        "pattern": "maven.oracle.com-libs-1-remote-cache/com/oracle/jdbc/ojdbc6/11.2.0.4/ojdbc6-11.2.0.4.jar",
                        "target": "./ojdbc6.jar"
                        }
                    ]
                    }"""
                    server.download(downloadSpec)
                    sh 'mv com/oracle/jdbc/ojdbc6/11.2.0.4/* ./jars'
                    sh 'rm -rf com'
                }
            }
        }
        stage('Docker') {
            steps {
                script {
                    withDockerRegistry(credentialsId: 'artifactory-user', url: 'https://artifacts.chemaxon.com') {
                    sh """
                        docker build -t ${pkg}:${REVISION} --build-arg VERSION=${REVISION} .
                        docker tag ${pkg}:${REVISION} ${env.DOCKER_HUB}/${dockerRepo}/${pkg}:${REVISION}
                        docker push ${env.DOCKER_HUB}/${dockerRepo}/${pkg}:${REVISION}

                        docker tag ${pkg}:${REVISION} ${env.DOCKER_HUB}/${dockerRepo}/${pkg}:latest
                        docker push ${env.DOCKER_HUB}/${dockerRepo}/${pkg}:latest
                    """
                    }                    
                }
            }
        }
    }
    post {
        always {
            script {
                if (buildIsStillSuccessful()) {
                    slackSend(
                            message: "${env.JOB_NAME} <${env.BUILD_URL}|Finished>.",
                            channel: slackThread,
                            color: "good"
                    )
                } else {
                    slackSend(
                            message: "${env.JOB_NAME} failed, please <${env.BUILD_URL}|check it>.",
                            channel: slackThread,
                            color: "danger"
                    )
                }
            }
            cleanWs()
            deleteDir()
        }
    }
}
